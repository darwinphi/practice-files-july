// function createElement (tag, config) {
// 	tag = tag || 'div'
// 	config = config || {}

// 	const element = document.createElement(tag)
// 	const content = config.content || 'Very default'
// 	const text = document.createTextNode(content)
// 	let classNames = config.classNames

// 	if (classNames === undefined) {
// 		classNames = ['module-text', 'default']
// 	}

// 	element.classList.add(...classNames)
// 	element.appendChild(text)

// 	return element
// }

function createElement(tag = 'div', {
	content = 'Default',
	classNames = ['module-text', 'special']
} = {}) {
	const element = document.createElement(tag)
	const text = document.createTextNode(content)
	element.classList.add(...classNames)
	element.appendChild(text)
	return element
}

const p = createElement()
const button = createElement('button', {
	content: 'Send',
	classNames: ['button', 'button--primary']
})

const el = document.querySelector("[data-js=element]")
el.appendChild(button)