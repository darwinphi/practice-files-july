class GreeterTraditional {
	constructor() {
		this.name = 'Darwin'
	}

	greet() {
		setTimeout(function() {
			console.log(`Hello ${this.name}`)
		}, 1000)
	}
}

let g = new GreeterTraditional()
g.greet() // Hello undefined

class GreeterBound {
	constructor() {
		this.name = 'Darwin'
	}

	greet() {
		setTimeout(function() {
			console.log(`Hello ${this.name}`)
		}.bind(this), 1000)
	}
}

let g4 = new GreeterBound()
g4.greet() // Hello Darwin

class GreeterArrow {
	constructor() {
		this.name = 'Darwin'
	}

	greet() {
		setTimeout(() => {
			console.log(`Hello ${this.name}`)
		}, 1000)
	}
}

let g3 = new GreeterArrow()
g3.greet() // Hello Darwin