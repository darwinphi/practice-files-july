// Imperative

function double (arr) {
  let results = []
  for (let i = 0; i < arr.length; i++){
    results.push(arr[i] * 2)
  }
  return results
}

console.log(double([1,2,3]))

const arr = [1,2,3]
function sum() {
	let result = 0
	for (let i = 0; i < arr.length; i++) {
		result = result + arr[i]
	}
	return result
}
console.log(sum())


// Declarative

let numbers = [1, 2, 4]

const add = numbers.reduce((total, number) => (total += number), 0) 

console.log(add)

console.log(numbers.map(number => number * 2))

// const btn = getElementById('btn')

// btn.addEventListener('click', function(e) {
// 	e.target.classList.toggle('highlight')
// })