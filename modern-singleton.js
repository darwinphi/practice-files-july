// const _data = [];
// const UserStore = {
// 	add: item => _data.push(item),
// 	get: id => _data.find(d => d.id === id)
// }

// Object.freeze(UserStore);

class UserStore {
	constructor() {
		if (! UserStore.instance) {
			this._data = []
			UserStore.instance = this	
		}
		
	}

	add(item) {
		this._data.push(item)
	}

	get(id) {
		return this._data.find(d => d.id === id);
	}
}

const instance = new UserStore();
Object.freeze(instance);

instance.add({id: 1, name: 'Darwin'})
instance.add({id: 2, name: 'Helen'})

console.log(instance.get(1))