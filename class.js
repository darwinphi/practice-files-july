// Old
function Person() {
	this.age = 0

	// ES5
	setInterval(function() {
		this.age++ // `this` refers to the global object
	}, 1000)

	// ES6
	setInterval(function() {
		this.age++ // `this refers to the person object`
	}, 1000)
}

// var p = new Person()

class Human {
	constructor(name) {
		this.name = name
	}

	greet() {
		return `Hello ${this.name}`
	}
}

const p = new Human('Darwin')
console.log(p.greet())
