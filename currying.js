let dragon = name => size => element => `${name} is a ${size} dragon that breathes ${element}`

// let dragon = function(name) {
// 	return function(size) {
// 		return function(element) {
// 			return name + 'is a ' + size + ' dragon that breathes ' + element
// 		}
// 	}
// }

console.log(dragon('Momo')('huge')('fire'))