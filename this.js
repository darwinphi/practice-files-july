// Implict Binding
// Explicit Binding
// new Binding
// window Binding

// Where is the function invoke?

var sayName = function (name) {
	console.log('Hello', name)
}

sayName('Darwin')

// Implicit Binding
// Look at the left of the dot at Call time `my.sayName`
var me = {
	name: 'Darwn',
	age: 22,
	sayName: function() {
		console.log(this.name)
	}
}
// me.sayName()

 var sayNameMixin = function(obj) {
 	obj.sayName = function() {
 		console.log(this.name)
 	}
 }

var me = {
	name: 'Darwin',
	age: 22
}

var you = {
	name: 'Joe',
	age: 25
}

sayNameMixin(me)
sayNameMixin(you)

me.sayName()
you.sayName()


// EXPLICIT BINDING
// .call, .apply, .bind

// separate function
var sayName = function(food1, food2, food3) {
	console.log('My name is ' + this.name + ' my fav foods are ' + food1)
}

var darwin = {
	name: 'Darwin',
	age: 22
}

var foods = ['banana', 'apple', 'burger']

// sayName will be invoke and the `this` keyword will reference to darwin
sayName.call(darwin, foods[0], foods[1], foods[2])

// same as call but you can pass the array variable instead of one by one
sayName.apply(darwin, foods)

// its like call but return new function
var newFunc = sayName.bind(darwin, foods[0], foods[1], foods[2])
newFunc()

// NEW BINDING
var Animal = function(color, name) {
	// When invoke in `new` keyword 
	// JS behind the scenes will create an object and save as `this` = {}
	this.color = color 
	this.name = name 
}

var dog = new Animal('black', 'Blacky')

// WINDOW BINDING
var sayAge = function() {
	// "use strict" - will have an error
	// otherwise its undefined
	console.log(this.age)
}

sayAge()
global.age = 69
sayAge()