class Animal {
	constructor(name = 'anonymous', legs = 4, noise = 'nothing') {
		this.type = 'animal'
		this.name = name 
		this.legs = legs
		this.noise = noise
	}

	speak() {
		console.log(`${this.name} says "${this.noise}"`)
	}

	walk() {
		console.log(`${this.name} walks on ${this.legs} legs`)	
	}

	set eats(food) {
		this.food = food
	}

	get dinner() {
		return `${this.name} eats ${this.food || 'nothing'} for dinner.`
	}
}

class Dog extends Animal {
	constructor(name) {
		super(name, 4, 'woof')
		this.type = 'dog'

		Dog.count++
	}

	speak(to) {
		super.speak()
		if (to) console.log(`to ${to}`)
	}

	static get COUNT() {
		return Dog.count
	}
}

const rex = new Dog('Rex');
rex.speak('everyone'); // Rex says "woof" to everyone
rex.eats = 'anything';
console.log( rex.dinner ); 

Dog.count = 0
console.log(`Dogs defined: ${Dog.COUNT}`); // Dogs defined: 0
const don = new Dog('Don');
console.log(`Dogs defined: ${Dog.COUNT}`); // Dogs defined: 1
const kim = new Dog('Kim');
console.log(`Dogs defined: ${Dog.COUNT}`); // Dogs defined: 2