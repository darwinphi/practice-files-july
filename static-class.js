// ESNext
// class MyClass {
// 	x = 1;
// 	y = 2;
// 	static z = 3;
// }
// console.log( MyClass.z ); // 3

class ES6Class {
	constructor() {
		this.x = 1;
		this.y = 2;
	}
}
ES6Class.z = 3;
console.log( ES6Class.z ); // 3